/*
The MIT License (MIT)

Copyright (c) 2015 Paweł Turkowski

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#pragma once
#include <type_traits>
#include <utility>
#include <string>
#include <functional>

template<class CharT, CharT... string>
struct StringLiteral;

template <class T, char... name>
struct CapturedArg : StringLiteral<char, name...>{

  T object;

  template<class Z>
  constexpr
  CapturedArg(Z&& initializer) : object(std::forward<Z>(initializer)){}

  constexpr
  operator T() && {
  	//return std::forward<T>(object);
  	return std::move(object);
  	//return object;
  }
  
  constexpr
  operator T() & {
  	return object;
  }
  
  constexpr
  T steal() && {
  	return std::move(object);
  }
  
  constexpr
  const T& get() & {
  	return object;
  }
  
  template<class Z>
  Z castTo() const { return object; }

};

template<class CharT, CharT... string>
struct StringLiteral{

  template<typename T> 
  using alias = T;

  template <class Arg>
  constexpr
  CapturedArg<typename std::decay<Arg>::type, string...> operator=(Arg&& arg) const {
    return std::forward<Arg>(arg);
  }

  constexpr
  std::true_type operator==(StringLiteral<CharT, string...>) const {
    return {};
  }
  
  template <int N>
  constexpr
  CharT getN(){
    return alias<CharT[]>{string...}[N];
  }
  
  constexpr
  std::basic_string<CharT> str() const {
    return {string...};
  }

  std::basic_string<CharT>& appendTo(std::basic_string<CharT>& outStr) const {
    outStr.append({string...});
    outStr.pop_back();
    return outStr;
  }
  
  template <class Stream>
  friend Stream& operator<< (Stream& out, const StringLiteral<CharT, string...>&){
  	CharT tmp[] = {string...};
  	out << tmp;
  	return out;
  }

  /**
   * Size in number of characters, without null terminator
   */
  constexpr
  size_t size() const {
    return sizeof(alias<CharT[]>{string...})/sizeof(CharT) - 1;
  }
};

/**
 * This is GCC extension (implemented also in clang).
 * Hope to see it in C++17
 */
template<class CharT, CharT... string>
constexpr
StringLiteral<CharT, string..., 0> operator ""_a(){
  return {};
}


/**
 * Types and/or names don't match, skip it, this usually begin recursion
 */
template <class T, char... argName, class Arg0, class... Args>
constexpr
T selector(CapturedArg<T, argName...>&& defArg, Arg0&& arg0, Args&&... args){
  return selector(std::forward<CapturedArg<T, argName...>>(defArg), std::forward<Args>(args)...);
}

/**
 * Name and type T match, return found argument.
 */
template <class T, char... argName, class... Args>
constexpr
T selector(CapturedArg<T, argName...>&& defArg, CapturedArg<T, argName...>&& arg0, Args&&... args){
  return std::forward<CapturedArg<T, argName...>>(arg0);
}

/**
 * Names match, types don't, but this overload is enabled only when Z can be converted to T
 */ 
template <class T, char... argName, class Z, class... Args>
constexpr
auto selector(CapturedArg<T, argName...>&& defArg, CapturedArg<Z, argName...>&& zArg, Args&&... args) -> decltype(typename std::enable_if<std::is_convertible<Z, T>{}, typename std::remove_reference<T>::type >::type{}){
  using UnrefT = typename std::remove_reference<T>::type;
  return zArg.template castTo<UnrefT>(); // convert Z to T
}

/**
 * Names match, but types don't, emit error
 */ 
template <class T, char... argName, class Z, class... Args>
constexpr
auto selector(CapturedArg<T, argName...>&& defArg, CapturedArg<Z, argName...>&&, Args&&... args) -> decltype(typename std::enable_if<!std::is_convertible<Z, T>{}, T>::type{}){
  constexpr bool _false = std::is_same<struct A, struct B>::value;
  static_assert(_false, "Arguments have same name but different types: Z is not convertible to T");
  return std::forward<CapturedArg<T, argName...>>(defArg);
}

/**
 * Nothig matched, return default argument
 */ 
template <class T, char... argName, char... name2>
constexpr
T selector(CapturedArg<T, argName...>&& defArg){
  return std::forward<CapturedArg<T, argName...>>(defArg);
}

template <class... Args>
auto forArgs(Args&&... args){
  return [&](auto f){
   int dummy[] = {(f(std::forward<Args>(args)), void(), 0)...};
  };
}


