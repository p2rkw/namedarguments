/*
The MIT License (MIT)

Copyright (c) 2015 Paweł Turkowski

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include "named-arguments.h"

#include <iostream>
#include <cstdio>

namespace std{
	std::string to_string(const char* str){
		return {str};
	}
}

/**
 * Naive python-like printf implementation using named argumets.
 *
 * Replace all occurences of {arg-name} found in format string with value 
 * bound to argument with same name.
 */
template <class... T>
void printf2(std::string format, T&&... args){
  std::string argName;
  forArgs(std::forward<T>(args)...)([&argName, &format](auto arg){
    argName.reserve(arg.size() + 3);
    argName = "{";
    arg.appendTo(argName);
    argName += "}";
    auto found = format.find(argName);
    while(found != std::string::npos){
      format.replace(found, argName.size(), std::to_string(arg.get()));
      found = format.find(argName);
    }
  });
  printf("%s", format.c_str());
}

template <class... T>
void bar(T&&... args){
  // shortcut to access the argument
  auto select = [&](auto&& defVal){
    return selector(std::move(defVal), std::forward<T>(args)...); 
  };

  // access arguments, and set default fallback values
  auto&& cat = select("cat"_a = "kitty");
  auto&& dog = select("dog"_a = "wolf");
  printf("bar: cat: %s; dog: %s\n", cat, dog);

  // python-like formatting:
  printf2("printf2: Another way of printing: dog: {dog}; cat: {cat}\n", std::forward<T>(args)...);
}

template <class... T>
void foo(T&&... args){
  auto select = [&](auto&& defVal){
    return selector(std::move(defVal), std::forward<T>(args)...); 
  };

  // argumetns may be accessed without shorthand:
  int baz = selector("baz"_a = -1, std::forward<T>(args)...);
  float asd = select("asd"_a = 4.2f);

  printf("foo: baz: %i, asd: %f\n", baz, asd);
  printf2("printf2: foo: baz: {baz}, asd: {asd}\n", std::forward<T>(args)...);

  // pass arguments to other varidic function
  bar(std::forward<T>(args)...);
  
  // print all argumets passed to function
  forArgs(std::forward<T>(args)...)([](auto arg){
    // using printf
  	printf("arg[%s] = %s\n", arg.str().c_str(), std::to_string(arg.get()).c_str());
    
    // or using streams
  	std::cout << "arg[" << arg << "] = " << arg.get() << std::endl;
  });
  puts("");
}

template <class... T>
void setDogName(T&&... args){
  using namespace std::string_literals;
  std::string outName;
  auto refWrapper = selector("dog"_a = std::ref(outName), std::forward<T>(args)...);
  std::string& dogNameRef = refWrapper;
  dogNameRef = "wolfik";
}

struct Rect{
  int x,y,w,h;
  void dump() const {
  	printf("rect: %i %i %i %i\n", x, y, w, h);
  }
};

template <class... T>
Rect makeRect(T&&... args){
  constexpr int defVal = 0;
  Rect r;
  r.x = selector("x0"_a = defVal, std::forward<T>(args)...);
  r.y = selector("y0"_a = 0, std::forward<T>(args)...);
  r.w = selector("x1"_a = 0, std::forward<T>(args)...) - r.x;
  r.h = selector("y1"_a = 0, std::forward<T>(args)...) - r.y;
  
  r.x = selector("x"_a = r.x, std::forward<T>(args)...);
  r.y = selector("y"_a = r.y, std::forward<T>(args)...);
  r.w = selector("w"_a = r.w, std::forward<T>(args)...);
  r.h = selector("h"_a = r.h, std::forward<T>(args)...);
  return r;
}

int main(){
  foo("width"_a = 123.0f, "height"_a = 1.5674f);
  
  // 87 will be converted to 87.f
  foo("asd"_a = 87, "cat"_a = "millar", "baz"_a = 18);

  int val = 568568;    // thanks to decay int& will be converted to int
  foo("cat"_a = "filon", "asd"_a = val);
  foo();
  
  bar("dog"_a = "pluto", "cat"_a = "meow");
  bar("dog"_a = "0");
  bar("pidgeon"_a = "burry");

  makeRect("x"_a = 10, "y"_a = 20, "w"_a = val, "h"_a = 40).dump();
  makeRect("x0"_a = 10, "y0"_a = 20, "x1"_a = 30, "y1"_a = 40).dump();

  // pass argument by reference
  std::string dogName = "goofy";
  setDogName("dog"_a = std::ref(dogName));
  printf("new dog name = %s\n", dogName.c_str());
  return 0;
}

