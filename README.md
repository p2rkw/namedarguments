# NamedArguments #

Type safe [named parameters](http://en.wikipedia.org/wiki/Named_parameter) implementation in C++14, using non standard [GCC extension](https://gcc.gnu.org/ml/gcc-patches/2013-04/msg00998.html).
Hope to see it in C++17 standard.


### Example usage ###

```
#!c++
template <class... T>
void make_widget(T&&... args){
  float width = selector("width"_a = 10.f, std::forward<T>(args)...);
  auto height = selector("height"_a = width, std::forward<T>(args)...); // type of 'height' will be deduced from default value (type of 'width' in this case)
  /* use width and height... */
}

make_widget("width"_a = 123.0f, "height"_a = 10.f); // rectangle
make_widget("width"_a = 20.0f); // square
make_widget(); // square with default size
make_widget("name"_a = "widget0"); // same as above - not recognized arguments are ignored
// make_widget("width"_a = "10"); // compilation error because of static assert fail - no known conversion from const char* to float

```
```
#!c++
printf2("{h} {w}! The answer: {answer}", "w"_a = "world", "answer"_a = 42, "h"_a = "Hello");
```